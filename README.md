<h1>Etapes à réaliser pour lancer le projet</h1>

<h2>Récupérer le projet</h2>
<p>Pour récupérer le projet via git, clonez le projet suivant : https://gitlab.com/axelvm/webreathe.git</p>

<h2>Installer les dépendances</h2>
<p>Pour installer les dépendances du projet, vous devez réaliser l'action dans les deux dossiers : frontend et backend</p>
<p>Accédez donc au deux dossiers en ligne de commande afin de réaliser l'action suivante : npm install</p>

<h2>Base de donnée</h2>

<p>Pour la gestion de base de données nous avons utilisé mysql</p>
<p>Lancez donc mysql et récupérez le fichier suivant <b>backend>public>database.sql</b></p>
<p>Récupérez donc le script SQL présent dans ce fichier et exécutez le</p>

<p><b>NB : Avec ce script, vous venez de créer toutes les tables utiles au lancement du projet et d'insérer l'utilisateur "admin admin" qui sera le seul utilisateur de la plateforme ayant le rôle "administrateur". Les autres éléments seront à créer lors de l'utilisation de la plateforme.</b></p>

<p>Pour relier le backend avec votre Base de donnée, accédez au fichier suivant : <b>backend>models>db.js</b></p>
<p>changez les informations de connexion à votre BDD.</p>

<h2>Lancement du projet</h2>

<p>Pour terminer, lancez la commande : "nodemon start" dans le dossier backend puis dans le dossier frontend en second temps</p>
<p>après avoir lancer la commande dans le dossier frontend l'URL : <b>http://localhost:3001/</b> s'ouvrira automatiquement dans votre navigateur.
</p>
<p>A partir de maintenant vous pouvez vous connecter au profile admin admin et ajouter des utilisateurs et vous connecter avec ceux ci.</p>


<h1>Estimations</h1>
<h2>Fonctionnalités</h2>
<li>Fonctionnalités restantes à réaliser :
    <ul>Connexion sécurisée : 3j (environ 24h)</ul>
    <ul>Gestion des accès sécurisée : 8h</ul>
    <ul>Gestion des photos de l'opération de maintenance : 5h</ul>
    <ul>Visualisation de l'état d'un véhicule dans le Tableau de bord : 4h</ul>
</li>
<li>Design à réaliser :
    <ul>Design Commun:  8h</ul>
    <ul>Design du Tableau de bord : 4h</ul>
</li>
<li>Vérifications :
    <ul>Tests Unitaires:  20h</ul>
    <ul>Sécurité : 8h</ul>
</li>
<li>Documentation API :
    <ul>API DOC:  4h</ul>
</li>

<h1>Avis personnel sur le Test</h1>
<h2>Critiques et intérêt</h2>
<p>Personnellement, j'ai beaucoup apprécié faire ce test, en effet, jusque maintenant je n'ai pu développer que dans le cadre de projets scolaires.
C'est donc pour moi une chance de pratiquer dans un cas un peu plus concret et par la suite avoir un retour.</p>
<p>D'autre part, j'ai trouvé qu'il était en effet long et qu'une semaine entière c'est beaucoup pour un test. Ceci dit ça permet de se dépasser et d'aller plus loin.</p>
<h2>Difficultés rencontrées</h2>

<p>Etant donné mon expérience assez faible en développement web, je n'ai pas utilisé l'outil que j'ai choisi au mieux de ses capacités notamment sur le fait de créer des composants réutilisables.</p>
<p>Le manque de temps ne m'a pas permis d'aller aussi loin que je le souhaitais en faisant le code le plus propre possible.</p>
<p>Le module de connexion étant également une grosse partie sur laquelle je n'ai que peu d'expérience, j'ai préféré la mettre de côté pour ce test et de créer une fonctionnalité alternative.</p>
