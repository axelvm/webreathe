'use strict';

var NotesModel = require('../models/notesModel.js');




exports.get_notes_of_operation = function(req, res) {
    var id = req.params;
    NotesModel.getNotesOfOperation(id, function(err, notes) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', notes);
        res.send(notes);
    });
};



exports.post_note_in_operation = function(req, res) {
    var new_note = new NotesModel(req.body);
    var id = req.params;

    if(!new_note.id_technicien || !new_note.note){

        //Vérification que tous les champs sont remplis

        res.status(400).send({ error:true, message: 'Ajoutez un technicien associé et un contenue à votre note' });

    }else {
        NotesModel.postNoteInOperation(id, new_note, function (err, note) {

            if (err) res.send(err);

            res.json(note);

        });
    }
};


