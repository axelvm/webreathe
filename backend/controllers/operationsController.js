'use strict';

var OperationsModel = require('../models/operationsModel.js');



exports.get_operations_by_id = function(req, res) {
  var id = req.params;
  OperationsModel.getOperationsById(id, function(err, operations) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', operations);
    res.send(operations);
  });
};

exports.get_operations_of_problem = function(req, res) {
    var id = req.params;
    OperationsModel.getOperationsOfProblem(id, function(err, operations) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', operations);
        res.send(operations);
    });
};


exports.post_operation_in_problem = function(req, res) {

    var id = req.params;
    var new_operation = new OperationsModel(req.body);

    if(!new_operation.sujet || !new_operation.description || !id || !new_operation.date_debut){

        //Vérification que tous les champs sont remplis

        res.status(400).send({ error:true, message: 'Please provide Operation starting date, subjet and description for an associated problem' });

    }else {
        OperationsModel.postOperationInProblem( id ,new_operation, function (err, operation) {

            if (err) res.send(err);

            res.json(operation);

        });
    }
};




exports.delete_operation_in_problem = function(req, res) {
    var id = req.params;
    OperationsModel.deleteOperationInProblem(id, function(err, Operation) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', Operation);
        res.send(Operation);
    });
};



exports.add_end_date_to_operation = function(req, res) {

    var id = req.params;
    var new_operation = new OperationsModel(req.body);

    if(!id || !new_operation.date_fin){

        //Vérification que tous les champs sont remplis

        res.status(400).send({ error:true, message: 'Please provide the ending date of the operation' });

    }else {
        OperationsModel.addEndDateToOperation( id ,new_operation, function (err, operation) {

            if (err) res.send(err);

            res.json(operation);

        });
    }
};

/*

exports.delete_probleme = function(req, res) {
  var id = req.params;
  ProblemesModel.deleteProbleme(id, function(err, Probleme) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', Probleme);
    res.send(Probleme);
  });
};
*/
