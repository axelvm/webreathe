'use strict';

var PiecesModel = require('../models/piecesModel.js');



exports.get_pieces = function(req, res) {
    PiecesModel.getPieces(function(err, pieces) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', pieces);
        res.send(pieces);
    });
};

exports.get_pieces_by_id = function(req, res) {
    var id = req.params;
    PiecesModel.getPiecesById(id, function(err, pieces) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', pieces);
        res.send(pieces);
    });
};



exports.get_pieces_of_operation = function(req, res) {
    var id = req.params;
    PiecesModel.getPiecesOfOperation(id, function(err, pieces) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', pieces);
        res.send(pieces);
    });
};


exports.post_piece = function(req, res) {
    var new_piece = new PiecesModel(req.body);

    if(!new_piece.nom){

        //Vérification que tous les champs sont remplis

        res.status(400).send({ error:true, message: 'Please provide a name to your piece' });

    }else {
        PiecesModel.postPiece(new_piece, function (err, piece) {

            if (err) res.send(err);

            res.json(piece);

        });
    }
};

exports.add_piece_to_operation = function(req, res) {
    var new_piece = new PiecesModel(req.body);
    var id = req.params;

    if(!new_piece.id_piece){

        //Vérification que tous les champs sont remplis

        res.status(400).send({ error:true, message: 'Please provide a piece to associate to the operation' });

    }else {
        PiecesModel.addPieceToOperation(id, new_piece, function (err, piece) {

            if (err) res.send(err);

            res.json(piece);

        });
    }
};



/*

exports.delete_operation_in_problem = function(req, res) {
    var id = req.params;
    OperationsModel.deleteOperationInProblem(id, function(err, Operation) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', Operation);
        res.send(Operation);
    });
};



exports.add_end_date_to_operation = function(req, res) {

    var id = req.params;
    var new_operation = new OperationsModel(req.body);

    if(!id || !new_operation.date_fin){

        //Vérification que tous les champs sont remplis

        res.status(400).send({ error:true, message: 'Please provide the ending date of the operation' });

    }else {
        OperationsModel.addEndDateToOperation( id ,new_operation, function (err, operation) {

            if (err) res.send(err);

            res.json(operation);

        });
    }
};



exports.delete_probleme = function(req, res) {
  var id = req.params;
  ProblemesModel.deleteProbleme(id, function(err, Probleme) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', Probleme);
    res.send(Probleme);
  });
};
*/
