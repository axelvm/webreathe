'use strict';

var ProblemesModel = require('../models/problemesModel.js');



exports.get_problems_of_vehicule = function(req, res) {
    var id = req.params;
  ProblemesModel.getProblemsOfVehicule(id, function(err, problemes) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', problemes);
        res.send(problemes);
    });
};

exports.get_problem_by_id = function(req, res) {
    var id = req.params;
    ProblemesModel.getProblemById(id, function(err, probleme) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', probleme);
        res.send(probleme);
    });
};

exports.create_problemes = function(req, res) {

  var id = req.params;
  var new_probleme = new ProblemesModel(req.body);

  if(!new_probleme.titre || !new_probleme.description || !id){

    //Vérification que tous les champs sont remplis

    res.status(400).send({ error:true, message: 'Please provide Probleme title and description and associated vehicule' });

  }else {
    ProblemesModel.createProbleme( id ,new_probleme, function (err, probleme) {

      if (err) res.send(err);

      res.json(probleme);

    });
  }
};

exports.delete_probleme = function(req, res) {
  var id = req.params;
  ProblemesModel.deleteProbleme(id, function(err, Probleme) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', Probleme);
    res.send(Probleme);
  });
};
