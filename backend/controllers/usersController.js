'use strict';

var UserModel = require('../models/usersModel.js');

exports.list_all_users = function(req, res) {
  UserModel.getAllUsers(function(err, User) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', User);
        res.send(User);
    });
};

exports.list_all_admins = function(req, res) {
  UserModel.getAllAdmins(function(err, User) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', User);
    res.send(User);
  });
};


exports.list_all_gestionnaires = function(req, res) {
  UserModel.getAllGestionnaires(function(err, User) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', User);
    res.send(User);
  });
};


exports.list_all_technicians = function(req, res) {
  UserModel.getAllTechnicians(function(err, User) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', User);
    res.send(User);
  });
};
exports.list_user_by_name_and_surname = function(req, res) {
  console.log("coucoucou", req.params);
  var user_to_search = new UserModel(req.params);
  UserModel.getUserByNameAndSurname(user_to_search, function(err, User) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', User);
    res.send(User);
  });
};

exports.list_user_by_id = function(req, res) {
  //console.log("coucoucou", req.params.id);
  var user_id = req.params;
  UserModel.getUserById(user_id, function(err, User) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', User);
    res.send(User);
  });
};

exports.delete_user = function(req, res) {
  //console.log("coucoucou", req.params.id);
  var user_id = req.params;
  UserModel.deleteUser(user_id, function(err, User) {

    console.log('controller')
    if (err)
      res.send(err);
    console.log('res', User);
    res.send(User);
  });
};

exports.create_a_user = function(req, res) {

    var new_user = new UserModel(req.body);

    if(!new_user.name || !new_user.surname || !new_user.role){

      //Vérification que tous les champs sont remplis

        res.status(400).send({ error:true, message: 'Please provide User name, surname and role' });

    }else {

      //On Vérifie l'existance de l'utilisateur dans la DB
      UserModel.getUserByNameAndSurname(new_user, function(err, User) {

        console.log('controller')
        if (err)
          res.send(err);
        if(User[0] && User[0].name){

          res.status(400).send({ error:true, message: 'This user already exist in the database', User });

        } else if(new_user.role !== "technicien" && new_user.role !== "admin" && new_user.role !== "gestionnaire"){

          //On Vérifie que le champs rôle est bien renseigné avec les valeurs acceptées

          res.status(400).send({ error:true, message: 'Please provide an existant role between "admin", "technicien" or "gestionnaire"' });

        }else{
          UserModel.createUser(new_user, function(err, user) {

            if (err) res.send(err);

            res.json(user);

          });
        }
        });
      //Création du nouvel Utilisateur


    }



};

