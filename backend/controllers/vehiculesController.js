'use strict';

var VehiculesModel = require('../models/vehiculesModel.js');

const typeVehicules = [
  "voiture",
  "camion",
  "moto",
  "avion",
  "bateau",
];


exports.list_all_vehicules = function(req, res) {
  VehiculesModel.getAllVehicules(function(err, Vehicule) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', Vehicule);
        res.send(Vehicule);
    });
};

exports.get_vehicule_by_id = function(req, res) {
    var id = req.params;
    VehiculesModel.getVehiculeById(id, function(err, Vehicule) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', Vehicule);
        res.send(Vehicule);
    });
};



exports.delete_vehicule = function(req, res) {
    //console.log("coucoucou", req.params.id);
    var vehicule_id = req.params;
    VehiculesModel.deleteVehicule(vehicule_id, function(err, Vehicule) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', Vehicule);
        res.send(Vehicule);
    });
};

exports.create_vehicules = function(req, res) {
    var new_vehicule = new VehiculesModel(req.body);

    if(!new_vehicule.type || !new_vehicule.date_achat){

      //Vérification que tous les champs sont remplis

      res.status(400).send({ error:true, message: 'Please provide Vehicule type and date dachat' });

    }else if(!typeVehicules.includes(new_vehicule.type)){
      res.status(400).send({ error:true, message: 'Entrez un véhicule autorisé svp'});
    }else {
      VehiculesModel.createVehicule(new_vehicule, function (err, vehicule) {

        if (err) res.send(err);

        res.json(vehicule);

      });
    }
  };
