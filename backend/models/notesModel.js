'vehicule strict';
var sql = require('./db.js');

//Vehicule object constructor
var Note = function(Note){
    this.note = Note.note;
    this.id_technicien = Note.id_technicien;
};




Note.getNotesOfOperation = function (id, result) {
    sql.query("Select * from note where id_operation = ?", id.id,function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('notes : ', res);

            result(null, res);
        }
    });
};


Note.postNoteInOperation = function (id, note, result) {
    sql.query(
        "INSERT INTO `note`set id_operation = ?, id_technicien = ?, note = ?",
        [id.id, note.id_technicien, note.note],
        function (err, res) {
            console.log(this.sql)
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }else{
                console.log('note : ', res);
                result(null, res);
            }
        });
};

module.exports= Note;
