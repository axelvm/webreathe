'vehicule strict';
var sql = require('./db.js');

//Vehicule object constructor
var Operation = function(Operation){
    this.id_probleme = Operation.id_probleme;
    this.date_debut = Operation.date_debut;
    this.date_fin = Operation.date_fin;
    this.sujet = Operation.sujet;
    this.description = Operation.description
};



Operation.getOperationsById = function (id, result) {
  sql.query("Select * from operations_maintenance where id = ?", id.id, function (err, res) {
    console.log(this.sql)
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('operations : ', res);

      result(null, res);
    }
  });
};

Operation.getOperationsOfProblem = function (id, result) {
    sql.query("Select * from operations_maintenance where id_probleme = ? and `date_fin` is null", id.id, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('operations : ', res);

            result(null, res);
        }
    });
};


Operation.postOperationInProblem = function (id, newOperation, result) {
    newOperation.date_debut = new Date(newOperation.date_debut);
    sql.query(
        "INSERT INTO `operations_maintenance` " +
        "set id_probleme = ?, " +
        "date_debut = ?, " +
        "sujet = ?, " +
        "description = ?;",
        [id.id, newOperation.date_debut, newOperation.sujet, newOperation.description],
        function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log('Operation : ', res);
            result(null, res);
        }
    });
};

Operation.addEndDateToOperation = function (id, date_fin, result) {
    date_fin.date_fin = new Date(date_fin.date_fin);
    sql.query(
        "UPDATE `operations_maintenance` SET `date_fin` = ? WHERE `operations_maintenance`.`id` = ? ;\n",
        [date_fin.date_fin, id.id],
        function (err, res) {
            console.log(this.sql)
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }else{
                console.log('Operation : ', res);
                result(null, res);
            }
        });
};
Operation.deleteOperationInProblem = function (id, result) {

    sql.query("DELETE FROM `operations_maintenance` WHERE `id` = ?", id.id, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log('Operation : ', res);
            result(null, res);
        }
    });
};

module.exports= Operation;
