'vehicule strict';
var sql = require('./db.js');

//Vehicule object constructor
var Piece = function(Piece){
    this.nom = Piece.nom;
    this.id_piece = Piece.id_piece;
    this.id_operation = Piece.id_operation;
};


Piece.getPieces = function (result) {
    sql.query("Select * from pieces", function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('pieces : ', res);
            result(null, res);
        }
    });
};


Piece.getPiecesById = function (id, result) {
    sql.query("Select * from pieces where id = ?", id.id, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('pieces : ', res);
            result(null, res);
        }
    });
};

Piece.getPiecesOfOperation = function (id, result) {
    sql.query("Select * from operation_piece where id_operation = ?", id.id,function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('pieces : ', res);

            result(null, res);
        }
    });
};


Piece.postPiece = function (newPiece, result) {
    sql.query(
        "INSERT INTO `pieces`set nom = ?", newPiece.nom, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log('pieces : ', res);
            result(null, res);
        }
    });
};

Piece.addPieceToOperation = function (id, piece, result) {
    sql.query(
        "INSERT INTO `operation_piece`set id_piece = ?, id_operation = ?",
        [piece.id_piece, id.id],
        function (err, res) {
            console.log(this.sql)
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }else{
                console.log('piece_operation : ', res);
                result(null, res);
            }
        });
};

/*
Operation.deleteOperationInProblem = function (id, result) {

    sql.query("DELETE FROM `operations_maintenance` WHERE `id` = ?", id.id, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log('Operation : ', res);
            result(null, res);
        }
    });
};
*/
module.exports= Piece;
