'vehicule strict';
var sql = require('./db.js');

//Vehicule object constructor
var Probleme = function(Probleme){
    this.titre = Probleme.titre;
    this.description = Probleme.description;
    this.id_vehicule = Probleme.id_vehicule
};


Probleme.getProblemsOfVehicule = function (id, result) {
    sql.query("Select * from problemes where id_vehicule = ?", id.id, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('problemes : ', res);

            result(null, res);
        }
    });
};
Probleme.getProblemById = function (id, result) {
    sql.query("Select * from problemes where id = ?", id.id, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('problemes : ', res);

            result(null, res);
        }
    });
};


Probleme.createProbleme = function (id, newProblem, result) {

  sql.query("INSERT INTO `problemes` set id_vehicule = ?, titre = ?, description = ?;", [id.id, newProblem.titre, newProblem.description], function (err, res) {
    console.log(this.sql)
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }else{
      console.log('Problemes : ', res);
      result(null, res);
    }
  });
};

Probleme.deleteProbleme = function (id, result) {

  sql.query("DELETE FROM `problemes` WHERE `problemes`.`id` = ?", id.id, function (err, res) {
    console.log(this.sql)
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }else{
      console.log('Problemes : ', res);
      result(null, res);
    }
  });
};


module.exports= Probleme;
