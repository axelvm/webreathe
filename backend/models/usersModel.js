'user strict';
var sql = require('./db.js');

//User object constructor
var User = function(User){
    this.name = User.name;
    this.surname = User.surname;
    this.role = User.role;
};

User.getAllUsers = function (result) {
    sql.query("Select * from users", function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('Users : ', res);

            result(null, res);
        }
    });
};


User.getAllAdmins = function (result) {
  sql.query("Select * from users where role = 'admin';", function (err, res) {

    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('Users : ', res);

      result(null, res);
    }
  });
};

User.getAllGestionnaires = function (result) {
  sql.query("Select * from users where role = 'gestionnaire';", function (err, res) {

    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('Users : ', res);

      result(null, res);
    }
  });
};


User.getAllTechnicians = function (result) {
  sql.query("Select * from users where role = 'technicien';", function (err, res) {

    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('Users : ', res);

      result(null, res);
    }
  });
};



User.getUserByNameAndSurname = function (user_to_search, result) {
  sql.query("Select * from users Where name = ? and surname = ?;", [user_to_search.name, user_to_search.surname], function (err, res) {
    console.log(this.sql)
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('Users : ', res);
      result(null, res);
    }
  });
};


User.getUserById = function (user_id, result) {
  sql.query("Select * from users Where id = ?;", user_id.id, function (err, res) {
    console.log(this.sql)
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('Users : ', res);
      result(null, res);
    }
  });
};



User.createUser = function (newUser, result) {

    sql.query("INSERT INTO `users` set ?;", newUser, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log('Users : ', res);
            result(null, res);
        }
    });
};

User.deleteUser = function (id, result) {

  sql.query("DELETE FROM `users` WHERE `users`.`id` = ?", id.id, function (err, res) {
    console.log(this.sql)
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }else{
      console.log('Users : ', res);
      result(null, res);
    }
  });
};


module.exports= User;
