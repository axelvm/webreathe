'vehicule strict';
var sql = require('./db.js');

//Vehicule object constructor
var Vehicule = function(Vehicule){
    this.type = Vehicule.type;
    this.date_achat = Vehicule.date_achat;
};

Vehicule.getAllVehicules = function (result) {
    sql.query("Select * from vehicules", function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('Vehicules : ', res);

            result(null, res);
        }
    });
};


Vehicule.getVehiculeById = function (id, result) {
    sql.query("Select * from vehicules where id = ?", id.id,function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('Vehicule : ', res);

            result(null, res);
        }
    });
};


Vehicule.createVehicule = function (newVehicule, result) {

  sql.query("INSERT INTO `vehicules` set ?;", newVehicule, function (err, res) {
    console.log(this.sql)
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }else{
      console.log('Vehicules : ', res);
      result(null, res);
    }
  });
};

Vehicule.deleteVehicule = function (id, result) {

    sql.query("DELETE FROM `vehicules` WHERE `vehicules`.`id` = ?", id.id, function (err, res) {
        console.log(this.sql)
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log('Vehicules : ', res);
            result(null, res);
        }
    });
};
module.exports= Vehicule;
