create database webreathe;

use webreathe;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `surname` varchar(16) NOT NULL,
  `role` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `users` (`id`, `name`, `surname`, `role`) VALUES (NULL, 'admin', 'admin', 'admin');


use webreathe;
CREATE TABLE IF NOT EXISTS `vehicules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(16) NOT NULL,
  `date_achat` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


use webreathe;
CREATE TABLE IF NOT EXISTS `problemes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_vehicule` int(10) unsigned NOT NULL,
   constraint fk_vehicule foreign key(id_vehicule)
    REFERENCES vehicules (id)
   ON UPDATE CASCADE
   ON DELETE CASCADE,
  `description` varchar(300) NOT NULL,
  `titre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



use webreathe;
CREATE TABLE IF NOT EXISTS `pieces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(50)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


use webreathe;
CREATE TABLE IF NOT EXISTS `operations_maintenance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_probleme` int(10) unsigned NOT NULL,
  `date_debut` DATETIME NOT NULL,
  `date_fin` DATETIME,
   `sujet` VARCHAR(300) NOT NULL ,
   `description` VARCHAR(300) NOT NULL ,
   constraint fk_probleme foreign key(id_probleme)
    REFERENCES problemes (id)
   ON UPDATE CASCADE
   ON DELETE CASCADE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `operation_piece` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_piece` int(10) unsigned NOT NULL,
  `id_operation` int(10) unsigned NOT NULL,
   constraint fk_piece foreign key(id_piece)
   REFERENCES pieces (id)
   ON UPDATE CASCADE
   ON DELETE CASCADE,
   constraint fk_operation foreign key(id_operation)
   REFERENCES operations_maintenance (id)
   ON UPDATE CASCADE
   ON DELETE CASCADE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `note` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_technicien` int(10) unsigned NOT NULL,
  `id_operation` int(10) unsigned NOT NULL,
  `note` VARCHAR(500) NOT NULL ,
   constraint fk_tehnicien foreign key(id_technicien)
   REFERENCES users (id)
   ON UPDATE CASCADE
   ON DELETE CASCADE,
   constraint fk_noteoperation foreign key(id_operation)
   REFERENCES operations_maintenance (id)
   ON UPDATE CASCADE
   ON DELETE CASCADE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
