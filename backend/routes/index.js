var express = require('express');
var router = express.Router();
var users = require('../controllers/usersController');
var vehicules = require('../controllers/vehiculesController');
var problemes = require('../controllers/problemesController');
var operations = require('../controllers/operationsController');
var pieces = require('../controllers/piecesController');
var notes = require('../controllers/notesController');

// USERS
/* GET users listing. */
router.get('/users', users.list_all_users);
router.get('/user/:name/:surname', users.list_user_by_name_and_surname);
router.get('/user/:id', users.list_user_by_id);
router.get('/users/admins', users.list_all_admins);
router.get('/users/gestionnaires', users.list_all_gestionnaires);
router.get('/users/techniciens', users.list_all_technicians);

/* POST user */
router.post('/users', users.create_a_user);

/* DELETE user */
router.delete('/user/:id', users.delete_user);


// VEHICULES
router.get('/vehicules', vehicules.list_all_vehicules);
router.post('/vehicules', vehicules.create_vehicules);
router.get('/vehicule/:id', vehicules.get_vehicule_by_id);
router.delete('/vehicule/:id', vehicules.delete_vehicule);


// PROBLEMES
router.get('/vehicules/:id/problemes', problemes.get_problems_of_vehicule);
router.get('/problemes/:id', problemes.get_problem_by_id);
router.post('/vehicules/:id/problemes', problemes.create_problemes);
router.delete('/problemes/:id', problemes.delete_probleme);

// OPERATIONS DE MAINTENANCE

router.get('/problemes/:id/operations', operations.get_operations_of_problem);
router.get('/operations/:id', operations.get_operations_by_id);
router.post('/problemes/:id/operations', operations.post_operation_in_problem);
router.delete('/operations/:id', operations.delete_operation_in_problem);
router.put('/operations/:id/datefin', operations.add_end_date_to_operation)


// PIECES
router.get('/pieces', pieces.get_pieces);
router.get('/pieces/:id', pieces.get_pieces_by_id);
router.post('/pieces', pieces.post_piece);

// PIECES FROM OPERATION
router.get('/operations/:id/pieces', pieces.get_pieces_of_operation);
router.post('/operations/:id/pieces', pieces.add_piece_to_operation);

// NOTES FROM OPERATION
router.get('/operations/:id/notes', notes.get_notes_of_operation);
router.post('/operations/:id/notes', notes.post_note_in_operation);


module.exports = router;



