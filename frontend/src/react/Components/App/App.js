import React, { Component } from 'react';

import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Connexion from "../pages/Connexion/Connexion";
import TableauDeBord from "../pages/TableauDeBord/TableauDeBord";
import Vehicule from "../pages/Vehicule/Vehicule";
import Operations from "../pages/Operations/Operations";
import DetailsOperation from "../pages/DetailsOperation/DetailsOperation"
import "antd/dist/antd.css";

class App extends Component {

    state={
        role: null,
        name: null,
        surname: null,
    }

    render() {
        return (

            <BrowserRouter>
                <Switch>
                        <Route
                            exact
                            path="/"
                            render={() => <Connexion setRole = {this.setRole} role={this.state.role}/>}
                        />
                        <Route
                            exact
                            path="/tableaudebord"
                            render={() => <TableauDeBord name={this.state.name} surname={this.state.surname} role={this.state.role}/>}
                        />
                        <Route
                            exact
                            path="/tableaudebord/vehicule/:id"
                            render={({match}) => <Vehicule id={match.params.id}/>}
                        />
                        <Route
                            exact
                            path="/tableaudebord/vehicule/:id_vehicule/problemes/:id_probleme/operations"
                            render={({match}) => <Operations idVehicule={match.params.id_vehicule} idProbleme={match.params.id_probleme}/>}
                        />
                        <Route
                          exact
                          path="/tableaudebord/vehicule/:id_vehicule/problemes/:id_probleme/operations/:id_operation"
                          render={({match}) => <DetailsOperation idVehicule={match.params.id_vehicule} idProbleme={match.params.id_probleme} idOperation={match.params.id_operation}/>}
                        />
                </Switch>
            </BrowserRouter>

        );
    }

  setRole = (user) => {
        this.setState({
          role: user.role,
          name: user.name,
          surname: user.surname,
        })
  }
}

export default App;
