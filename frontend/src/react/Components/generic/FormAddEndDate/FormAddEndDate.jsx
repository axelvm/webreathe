import React from "react";
import {Form, Button,DatePicker} from 'antd';




class FormAddEndDate extends React.PureComponent {

    handleSubmitEndDate = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
                var date_debut = new Date(this.props.itemSelected.date_debut)
                if(date_debut < values.endDate._d) {
                    this.props.validateArchive(this.props.itemSelected.id, values)
                    }else{
                    console.log("date de fin est inférieur à la date de début");
                    }
        });
    }


    render(){
        const { getFieldDecorator } = this.props.form;
        return(
        <Form onSubmit={this.handleSubmitEndDate}>
            <Form.Item>
                {getFieldDecorator('endDate', {
                    rules: [{ required: true, message: 'Veuillez remplir la date de fin' }],
                })(
                    <DatePicker onChange={this.onChange} />
                )}
            </Form.Item>
            <Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Archiver
                    </Button>
                </Form.Item>

            </Form.Item>
        </Form>
        )
    }
}
const WrappedAddEndDate = Form.create()(FormAddEndDate)

export default (WrappedAddEndDate)


