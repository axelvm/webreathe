import React from "react";
import classNames from "./formNewNote.module.css";
import {Form, Button, Input, Typography, Select} from 'antd';


const { TextArea } = Input;
const { Title } = Typography;
const { Option } = Select;


class FormNewNote extends React.PureComponent {


  state = {
    affectedPiece: null,
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.addNote(values, this.props.idOperation)

      }
    });
  }

  addOperation = (idProbleme, values) => {
    //this.props.addOperation(idProbleme, values, this.state.affectedPiece);

  }


  render() {


    const {getFieldDecorator} = this.props.form;
    return (
      <div>


        <Form onSubmit={this.handleSubmit} className={classNames.corps}>
          <Title className={classNames.title} level={4}>Nouvelle note</Title>

          <Form.Item className={classNames.inputItem}>
            {getFieldDecorator('note', {
              rules: [{required: true, message: 'Veuillez ajouter un contenu à votre note'}],
            })(
              <TextArea rows={4} placeholder="contenu de la note"/>
            )}
          </Form.Item>

          <Form.Item className={classNames.inputItem}>
            {getFieldDecorator('id_technicien', {
              rules: [{required: true, message: 'Veuillez associer un technicien'}],
            })(
            <Select
              style={{ width: 120 }}
            >
              {this.props.techniciens.map(technicien => (
                <Option key={technicien.id}>{technicien.name}</Option>
              ))}
            </Select>
            )}
          </Form.Item>
          <Form.Item className={classNames.buttonsItem}>

            <Button type="primary" htmlType="submit" className="login-form-button">
              Ajouter
            </Button>

          </Form.Item>

        </Form>
      </div>
    )
  }

  addPieceToOperation = (piece) => {
    this.setState({
      affectedPiece: piece,
    })
  }
}

const WrappedNewNote = Form.create()(FormNewNote)

export default (WrappedNewNote)


