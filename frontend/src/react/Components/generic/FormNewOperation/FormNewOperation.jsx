import React from "react";
import classNames from "./formNewOperation.module.css";
import {Form, Button, Input, Icon, Typography, DatePicker} from 'antd';
import SelectPiece from "../SelectPiece/SelectPiece";
const { TextArea } = Input;
const { Title } = Typography;
class FormNewOperation extends React.PureComponent {


  state = {
    affectedPiece: null,
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.addOperation(this.props.problemeId[0], values)

      }
    });
  }

  addOperation = (idProbleme, values) => {
    this.props.addOperation(idProbleme, values, this.state.affectedPiece);

  }


  render() {


    const {getFieldDecorator} = this.props.form;
    return (
      <div>


        <Form onSubmit={this.handleSubmit} className={classNames.corps}>
          <Title className={classNames.title} level={4}>Formulaire de création d'une opération de maintenance</Title>
          <Form.Item className={classNames.inputItem}>
            {getFieldDecorator('sujet', {
              rules: [{required: true, message: 'Veuillez entrer le titre du problème'}],
            })(
              <Input prefix={<Icon type="user" className={classNames.inputIconUser}/>} placeholder="Sujet"/>
            )}
          </Form.Item>
          <Form.Item className={classNames.inputItem}>
            {getFieldDecorator('description', {
              rules: [{required: true, message: 'Veuillez ajouter une description à votre problème'}],
            })(
              <TextArea rows={4} placeholder="Description"/>
            )}
          </Form.Item>
          <Form.Item className={classNames.inputItem}>
            {getFieldDecorator('date_debut', {
              rules: [{required: true, message: 'Veuillez ajouter une date de début de l\'opération'}],
            })(
              <DatePicker placeholder="date de début"/>
            )}
          </Form.Item>
          <Form.Item className={classNames.inputItem}>

              <SelectPiece addPieceToOperation={this.addPieceToOperation}/>

          </Form.Item>
          <Form.Item className={classNames.buttonsItem}>

            <Button type="primary" htmlType="submit" className="login-form-button">
              Add
            </Button>

          </Form.Item>

        </Form>
      </div>
    )
  }

  addPieceToOperation = (piece) => {
    this.setState({
      affectedPiece: piece,
    })
  }
}

const WrappedNewOperation = Form.create()(FormNewOperation)

export default (WrappedNewOperation)


