import React from "react";
import classNames from "./formNewProbleme.module.css";
import {Form, Button, Input, Icon, Typography} from 'antd';

const { TextArea } = Input;
const { Title } = Typography;
class FormNewProbleme extends React.PureComponent {



    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
            {
                this.addProbleme(values)

            }
        });
    }

  addProbleme = (values) =>{
      this.props.addProbleme(values);
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        return (
           <div>


             <Form onSubmit={this.handleSubmit} className={classNames.corps}>
               <Title className={classNames.title} level={4}>Formulaire de création d'un problème</Title>
               <Form.Item className={classNames.inputItem}>
                   {getFieldDecorator('titre', {
                       rules: [{ required: true, message: 'Veuillez entrer le titre du problème' }],
                   })(
                       <Input prefix={<Icon type="user" className={classNames.inputIconUser}/>} placeholder="Titre"/>
                   )}
               </Form.Item>
               <Form.Item className={classNames.inputItem}>
                   {getFieldDecorator('description', {
                       rules: [{ required: true, message: 'Veuillez ajouter une description à votre problème' }],
                   })(
                     <TextArea rows={4} placeholder="Description"/>
                   )}
               </Form.Item>
               <Form.Item className={classNames.buttonsItem}>

                   <Button type="primary" htmlType="submit" className="login-form-button">
                       Add
                   </Button>

               </Form.Item>

           </Form>
           </div>
        )
    }

}
const WrappedNewProblem = Form.create()(FormNewProbleme)

export default (WrappedNewProblem)


