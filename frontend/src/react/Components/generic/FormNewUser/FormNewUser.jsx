import React from "react";
import classNames from "./formNewUser.module.css";
import {Form, Button, Input, Icon, Radio, Typography} from 'antd';
const { Title } = Typography;




class FormNewUser extends React.PureComponent {

    state = {
        value: "technicien",
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
            {
                values.role = this.state.value;
                this.addUser(values)

            }
        });
    }

    addUser = (values) =>{

      this.props.addUser(values);

    }


    onChange = e => {
        this.setState({
            value: e.target.value,
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
           <div>
               <Form onSubmit={this.handleSubmit} className={classNames.corps}>
                 <Title className={classNames.title} level={4}>Formulaire de création d'un utilisateur</Title>
                 <Form.Item className={classNames.inputItem}>
                   {getFieldDecorator('name', {
                       rules: [{ required: true, message: 'Veuillez mettre votre Prénom!' }],
                   })(
                       <Input prefix={<Icon type="user" className={classNames.inputIconUser}/>} placeholder="Prénom"/>
                   )}
               </Form.Item>
               <Form.Item className={classNames.inputItem}>
                   {getFieldDecorator('surname', {
                       rules: [{ required: true, message: 'Veuillez mettre votre Nom' }],
                   })(
                       <Input prefix={<Icon type="user" className={classNames.inputIconUser}/>} placeholder="Nom"/>
                   )}
               </Form.Item>
               <Form.Item className={classNames.radiogroup}>
                   <Radio.Group  onChange={this.onChange} value={this.state.value}>
                       <Radio value={"gestionnaire"}>
                            Gestionnaire
                       </Radio>
                       <Radio value={"technicien"}>
                           Technicien
                       </Radio>
                   </Radio.Group>
               </Form.Item>
                   <Form.Item className={classNames.buttonsItem}>

                       <Button type="primary" htmlType="submit" className="login-form-button">
                           Add
                       </Button>

                   </Form.Item>

               </Form>
           </div>
        )
    }

}
const WrappedNewUser = Form.create()(FormNewUser)

export default (WrappedNewUser)


