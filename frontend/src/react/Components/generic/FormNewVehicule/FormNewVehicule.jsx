import React from "react";
import classNames from "./formNewVehicule.module.css";
import {Form, Button, Input, Icon, DatePicker, Typography} from 'antd';

const { Title } = Typography;


class FormNewVehicule extends React.PureComponent {

    state = {
      date_achat: null,
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
            {
                values.date_achat = this.state.date_achat;
                this.addVehicule(values)

            }
        });
    }

  addVehicule = (values) =>{
      this.props.addVehicule(values);
    }

  onChange = (date, dateString) =>  {
    this.setState({
      date_achat: dateString,
    })
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
           <div>
               <Form onSubmit={this.handleSubmit} className={classNames.corps}>
                 <Title className={classNames.title} level={4}>Formulaire de création d'un Véhicule</Title>

                 <Form.Item className={classNames.inputItem}>
                   {getFieldDecorator('type', {
                       rules: [{ required: true, message: 'Veuillez entrer le type de véhicule ("camion", "voiture" ou "moto")!' }],
                   })(
                       <Input prefix={<Icon type="user" className={classNames.inputIconUser}/>} placeholder="Type"/>
                   )}
               </Form.Item>
               <Form.Item className={classNames.inputItem}>
                   {getFieldDecorator('date_achat', {
                       rules: [{ required: true, message: 'Veuillez sélectionner la date dachat du véhicule' }],
                   })(
                     <DatePicker onChange={this.onChange} placeholder="Sélectionner la date d'achat du véhicule"/>
                   )}
               </Form.Item>
               <Form.Item className={classNames.buttonsItem}>

                   <Button type="primary" htmlType="submit" className="login-form-button">
                       Add
                   </Button>

               </Form.Item>

           </Form>
           </div>
        )
    }

}
const WrappedNewVehicule = Form.create()(FormNewVehicule)

export default (WrappedNewVehicule)


