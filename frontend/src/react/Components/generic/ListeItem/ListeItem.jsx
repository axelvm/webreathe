import React from "react";
import classNames from "./listeItem.module.css";
import { List, Button } from 'antd';



export default class ListeItem extends React.PureComponent {



  render() {
    return (

              <List.Item className={classNames.list} key={this.props.item.id}>
                <div>{this.props.item.name + " "+ this.props.item.surname}</div>
                <Button key={this.props.item.id} onClick={this.onDelete} type="link">delete</Button>
              </List.Item>

    )
  }

  onDelete = () => {
    this.props.onDelete(this.props.item);
  }

}


