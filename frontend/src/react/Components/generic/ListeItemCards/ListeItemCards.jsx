import React from "react";
import classNames from "./listeItemCards.module.css";
import {List, Card, Button} from 'antd';
import { Link } from "react-router-dom";



export default class ListeItemCards extends React.PureComponent {



  render() {
    return (

      <List.Item>
        <Card title={this.props.item.type}
                extra={this.getExtra()}>
                Date d'achat : {this.props.item.date_achat}
        </Card>

      </List.Item>

    )
  }


  getExtra = () => {
      if(this.props.status === "gestionnaire"){
          return (<div className={classNames.options}>
                    <Button size="small" key={this.props.item.id} onClick={this.onDelete} type="link">delete</Button>
                    <Link to={"/tableaudebord/vehicule/"+this.props.item.id}><Button size="small" key={this.props.item.id} type="link">select</Button></Link>
                  </div>
              )
      }else if(this.props.status === "technicien"){
          return <Link to={"/tableaudebord/vehicule/"+this.props.item.id}><Button key={this.props.item.id} type="link">details</Button></Link>
      }

  }
  onDelete = () => {
    this.props.onDelete(this.props.item);
  }



}


