import React from "react";
import classNames from "./listeUtilisateurs.module.css";
import { List } from 'antd';
import ListeItem from '../ListeItem/ListeItem'



export default class ListeUtilisateurs extends React.PureComponent {


  state = {
    list: [],
  }


  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.list !== this.props.list){
      this.setState({
        list: this.props.list
      })
    }
  }

  render() {
    return (
      <div className={classNames.corps}>
        <List
          bordered
          header={<h2>{this.props.title}</h2>}
          dataSource={this.state.list}
          renderItem={item => (
              <ListeItem className={classNames.list} onDelete={this.onDelete} item={item}/>
          )}
      />
      </div>
    )
  }

  onDelete = (item) => {
    this.props.deleteUser(item);
  }

}


