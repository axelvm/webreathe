import { List } from 'antd';
import React from "react";
import classNames from "./listeVehicules.module.css";
import ListeItemCards from "../ListeItemCards/ListeItemCards";


export default class ListeVehicules extends React.PureComponent {

  state = {
    list: [],
  }


  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.list !== this.props.List){
      this.setState({
        list: this.props.list
      })
    }
  }
  render() {
    return (
      <div className={classNames.corps}>
        <List
          className={classNames.list}
          grid={{ gutter: 30, column: 4}}
          dataSource={this.state.list}
          renderItem={item => (
            <ListeItemCards status={this.props.status} onDelete={this.onDelete} onSelect={this.onSelect} item={item}/>
          )}

        />
      </div>
    )
  }
  onDelete = (item) => {
    if(this.props.deleteVehicule){
      this.props.deleteVehicule(item);
    }
  }

  onSelect = (item) => {
    if(this.props.selectVehicule){
      this.props.selectVehicule(item);
    }
  }
}
