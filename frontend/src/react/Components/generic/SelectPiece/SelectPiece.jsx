import React from "react";
import { AutoComplete } from 'antd';
import {getPieces, postPiece} from "../../../../redux/pieces/actions";
import connect from "react-redux/es/connect/connect";


class SelectPiece extends React.PureComponent {

  state = {
    dataSource: [],
  };

    componentWillMount(){
      this.props.getPieces();

    }



  componentDidUpdate(prevProps, prevState, snapshot) {
      if(prevProps.pieces !== this.props.pieces){
        let tablePiece = []
        this.props.pieces.map((item) => {
          tablePiece.push(item.nom);
          return null;
        })
        this.setState({
          dataSource : tablePiece})
      }
    }

    render() {
        return (
           <div>
             <AutoComplete
               dataSource={this.state.dataSource}
               style={{ width: 200 }}
               filterOption={(inputValue, option) =>
                 option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
               }
               onBlur={this.onSelect}
               placeholder="input here"
             />
           </div>
        )
    }

    onSelect = (value) => {
      if(this.state.dataSource.includes(value)){
        this.props.pieces.map((piece) => {
          if(piece.nom === value){
            this.props.addPieceToOperation(piece.id)
          }
          return null;
        })
      }else{
        if(value){
          this.props.postPiece(value)
          this.props.getPieces()
        }

      }
    }

}




const mapStateToProps = state  => ({
    pieces : state.pieces.map,
  }
)

const mapDispatchToProps = {
  getPieces,
  postPiece,

}


export default connect(mapStateToProps, mapDispatchToProps)(SelectPiece)