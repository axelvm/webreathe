import React from "react";
import {Form, Icon, Input, Button, Typography} from 'antd';
import classNames from "./connexion.module.css";
import connect from "react-redux/es/connect/connect";
import { connectUser } from "../../../../redux/identification/actions";
import { Redirect } from "react-router-dom";
const { Title } = Typography;



class Connexion extends React.PureComponent {

  componentDidUpdate(prevProps){
    if(this.props.isLogIn && this.props.isLogIn !== prevProps.isLogIn){
      this.setState({
        isLogIn: this.props.isLogIn[0],
      })
    }
  }
    state = {
      isLogIn: false,
    }



    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err)
            {
              this.identificateUser(values)
            }
        });
    }

     identificateUser = (values) => {

         this.props.connectUser(values);

    }

    render() {

      if(this.state.isLogIn.role){
        this.props.setRole(this.state.isLogIn)
        return <Redirect to={'/tableaudebord'}/>
      }else{
        const { getFieldDecorator } = this.props.form;
        return (

          <Form onSubmit={this.handleSubmit} className={classNames.corps}>
            <Title className={classNames.title} level={4}>Formulaire d'identification</Title>
            <Form.Item className={classNames.inputItem}>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Veuillez mettre votre Prénom!' }],
              })(
                <Input prefix={<Icon type="user" className={classNames.inputIconUser}/>} placeholder="Prénom"/>
              )}
            </Form.Item>
            <Form.Item className={classNames.inputItem}>
              {getFieldDecorator('surname', {
                rules: [{ required: true, message: 'Veuillez mettre votre Nom' }],
              })(
                <Input prefix={<Icon type="user" className={classNames.inputIconUser}/>} placeholder="Nom"/>
              )}
            </Form.Item>
            <Form.Item className={classNames.buttonsItem}>

              <Button type="primary" htmlType="submit" className="login-form-button">
                Connexion
              </Button>

            </Form.Item>
          </Form>
        );
      }

    }

};

const WrappedLogin = Form.create()(Connexion)

const mapStateToProps = state  => ({
    isLogIn : state.isLogIn.map,
  }
)

const mapDispatchToProps = {
  connectUser,
}


export default connect(mapStateToProps, mapDispatchToProps)(WrappedLogin)
