import React from "react";
import {getProblemeById} from "../../../../redux/problemes/actions";
import {getPieceById} from "../../../../redux/pieces/actions";
import {getOperationById} from "../../../../redux/operations/actions";
import { getNotesOfOperation, postNoteToOperation } from "../../../../redux/notesOfOperation/actions";
import { getPiecesOfOperation } from "../../../../redux/piecesOfOperation/actions";
import connect from "react-redux/es/connect/connect";
import {List, Typography} from "antd";
import classNames from "./detailsOperation.module.css";
import dateFormat from "dateformat";
import ListeNotesCards from "../../generic/ListeNotesCards/ListeNotesCards";
import FormNewNote from "../../generic/FormNewNote/FormNewNote";
import {getTechniciens} from "../../../../redux/techniciens/actions";


const { Title } = Typography;



class DetailsOperation extends React.PureComponent {

  state={
    operation: [],
    notes: [],
    listTechniciens: [],
    piecesOfOperation: [],
    pieces: [],
  }
    componentWillMount(){
     this.props.getOperationById(this.props.idOperation);
      this.props.getNotesOfOperation(this.props.idOperation);
      this.props.getTechniciens();
      this.props.getPiecesOfOperation(this.props.idOperation)
    }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.operation !== this.props.operation){
      this.setState({
        operation: this.props.operation,
      })
    }
    if(prevProps.notes !== this.props.notes){
      this.setState({
        notes: this.props.notes,
      })
    }
    if(prevProps.listTechniciens !== this.props.listTechniciens){
      this.setState({
        listTechniciens: this.props.listTechniciens,
      })
    }
    if(prevProps.piecesOfOperation !== this.props.piecesOfOperation){
      this.setState({
        piecesOfOperation: this.props.piecesOfOperation,
      }, () => {
        this.getPiecesOfOperation()
      })
    }
    if(prevProps.piece !== this.props.piece){
        this.addPieceToTable(this.props.piece[0]);
    }
  }

    render() {

    return(
      <div className={classNames.container}>
        <div className={classNames.corps}>
          <Title className={classNames.title}> Opération de maintenance : {this.state.operation.map((item) => {return item.sujet})}</Title>
          <div className={classNames.dates}>
            <div>date de début : {this.state.operation.map((item) => {return dateFormat(item.date_debut, "dd/mm/yyyy")})}</div>
            {this.state.operation.map((item) => {
              if(item.date_fin){
                return (<div>date de fin : {this.state.operation.map((item) => {return dateFormat(item.date_fin, "dd/mm/yyyy")})}</div>)
              }else{
                return (<div>En cours</div>)
              }
            })}
          </div>
          <div className={classNames.secondPart}>
            <div className={classNames.description}>
              <Title className={classNames.title} level={4}>Description de l'opération</Title>
              {this.state.operation.map((item) => {return item.description})}
              <Title className={classNames.title} level={4}>Pieces affectées</Title>
              {this.state.pieces.map((piece) => {
                return (<p>{piece.nom}</p>)
              })}

            </div>
            <FormNewNote techniciens={this.state.listTechniciens} idOperation={this.props.idOperation} addNote={this.addNote}/>
          </div>
        </div>
        <List
          className={classNames.list}
          grid={{ gutter: 30, column: 4}}
          dataSource={this.state.notes}
          renderItem={item => (
            <ListeNotesCards item={item}/>
          )}

        />


      </div>
    )


    }
  addNote = (note, idOperation) => {
    this.props.postNoteToOperation(idOperation, note);
    this.props.getNotesOfOperation(this.props.idOperation);

  }

  getPiecesOfOperation = () => {
    this.props.piecesOfOperation.map((item) => {
      this.props.getPieceById(item.id_piece);
      return null
    })
  }

  addPieceToTable = (piece) => {
    let newPieces = [...this.state.pieces];
    newPieces.push(piece)
    this.setState({
      pieces: newPieces
    })
  }

}




const mapStateToProps = state  => ({
        probleme : state.probleme.map,
        operation: state.operation.map,
        piecesOfOperation: state.piecesOfOperation.map,
        notes: state.notesOfOperation.map,
        listTechniciens: state.listTechniciens.map,
        piece: state.piece.map,
    }
)

const mapDispatchToProps = {
    getProblemeById,
    getOperationById,
    getNotesOfOperation,
    postNoteToOperation,
    getTechniciens,
    getPiecesOfOperation,
    getPieceById,
}


export default connect(mapStateToProps, mapDispatchToProps)(DetailsOperation)
