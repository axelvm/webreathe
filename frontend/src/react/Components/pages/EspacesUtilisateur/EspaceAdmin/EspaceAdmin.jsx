import React from "react";
import ListeUtilisateurs from '../../../generic/ListeUtilisateurs/ListeUtilisateurs'
import FormNewUser from '../../../generic/FormNewUser/FormNewUser'
import classNames from './espaceAdmin.module.css';
import {getGestionnaires} from "../../../../../redux/gestionnaires/actions";
import {getTechniciens} from "../../../../../redux/techniciens/actions";
import { Typography  } from 'antd'
import connect from "react-redux/es/connect/connect";
import {postUser} from "../../../../../redux/utilisateurs/actions";
import {deleteUser} from "../../../../../redux/utilisateurs/actions"

const { Title } = Typography;
class EspaceAdmin extends React.PureComponent {


  componentDidMount(){
    this.props.getGestionnaires();
    this.props.getTechniciens();
  }

  addUser = (values) => {
    this.props.postUser(values);
    if(values.role === "gestionnaire"){
      this.props.getGestionnaires();
    }else if(values.role === "technicien"){
      this.props.getTechniciens();
    }
  }

  deleteUser = (item) => {
    this.props.deleteUser(item.id);
    if(item.role === "gestionnaire"){
      this.props.getGestionnaires();
    }else if(item.role === "technicien"){
      this.props.getTechniciens();
    }
  }

  render(){

    return (
      <div>
      <Title className={classNames.title}> Bienvenue sur le tableau de bord Administrateur  </Title>
        <Title className={classNames.title} level={2}>{this.props.name}</Title>
        <div>
          <FormNewUser addUser={this.addUser} />
        </div>
        <div className={classNames.listes}>
          <ListeUtilisateurs className={classNames.liste} title="Les Gestionnaires" list={this.props.listGestionnaires} deleteUser={this.deleteUser}/>
          <ListeUtilisateurs className={classNames.liste} title="Les Techniciens" list={this.props.listTechniciens} deleteUser={this.deleteUser}/>
        </div>

      </div>

    )
  }
}
const mapStateToProps = state  => ({
      listGestionnaires : state.listGestionnaires.map,
      listTechniciens : state.listTechniciens.map,
      newUser: state.newUser.map,
      //result: state.result.map,
    }
)

const mapDispatchToProps = {
  getGestionnaires,
  getTechniciens,
  postUser,
  deleteUser,
}


export default connect(mapStateToProps, mapDispatchToProps)(EspaceAdmin)

