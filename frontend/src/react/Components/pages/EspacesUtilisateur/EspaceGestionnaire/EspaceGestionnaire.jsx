import React from "react";
import classNames from "../EspaceAdmin/espaceAdmin.module.css";
import FormNewVehicule from "../../../generic/FormNewVehicule/FormNewVehicule";
import ListeVehicules from "../../../generic/ListeVehicules/ListeVehicules";
import {Typography} from "antd";
import {getVehicules, postVehicules, deleteVehicule} from "../../../../../redux/vehicules/actions";
import connect from "react-redux/es/connect/connect";


const { Title } = Typography;


class EspaceGestionnaire extends React.PureComponent {
  componentDidMount(){
    this.props.getVehicules();
  }
  render(){
    return (
      <div>
        <Title className={classNames.title}> Bienvenue sur le tableau de bord Gestionnaire  </Title>
        <Title className={classNames.title} level={2}>{this.props.name}</Title>
        <div>
          <FormNewVehicule addVehicule={this.addVehicule} />
        </div>
        <div>
          <ListeVehicules className={classNames.liste} status={"gestionnaire"} list={this.props.vehicules} deleteVehicule={this.deleteVehicule}/>
        </div>
      </div>
    )
  }

  addVehicule = (values) => {
    this.props.postVehicules(values);
    this.props.getVehicules();
  }

  deleteVehicule = (item) => {
    this.props.deleteVehicule(item.id);
    this.props.getVehicules();
  }
}


const mapStateToProps = state  => ({
    vehicules : state.vehicules.map,
    resultVehicule : state.resultVehicule.map,
  }
)

const mapDispatchToProps = {
  getVehicules,
  postVehicules,
  deleteVehicule,
}


export default connect(mapStateToProps, mapDispatchToProps)(EspaceGestionnaire)
