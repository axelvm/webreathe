import React from "react";
import classNames from "../EspaceAdmin/espaceAdmin.module.css";
import ListeVehicules from "../../../generic/ListeVehicules/ListeVehicules";
import {Typography} from "antd";
import {getVehicules} from "../../../../../redux/vehicules/actions";
import connect from "react-redux/es/connect/connect";
import { Redirect } from "react-router-dom";


const { Title } = Typography;


class EspaceTechnicien extends React.PureComponent {
  componentDidMount() {
    this.props.getVehicules();
  }

  render() {
    return (
        <div>
          <Title className={classNames.title}> Bienvenue sur le tableau de bord Technicien </Title>
          <Title className={classNames.title} level={2}>{this.props.name}</Title>

          <div>
            <ListeVehicules
              className={classNames.liste}
              status="technicien"
              list={this.props.vehicules}
                            selectVehicule={this.selectVehicule}/>
          </div>
        </div>
    )
  }

  selectVehicule = (item) => {
    return <Redirect to={"/tableaudebord/vehicule/" + item.id}/>
  }

}
const mapStateToProps = state  => ({
      vehicules : state.vehicules.map,
    }
)

const mapDispatchToProps = {
  getVehicules,
}


export default connect(mapStateToProps, mapDispatchToProps)(EspaceTechnicien)
