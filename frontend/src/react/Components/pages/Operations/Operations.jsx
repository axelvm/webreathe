import React from "react";
import {getProblemeById} from "../../../../redux/problemes/actions";
import {getPendingOperationInProbleme, postOperationInProbleme, addEndDateOperation} from "../../../../redux/operations/actions";
import {postPieceToOperation} from "../../../../redux/piecesOfOperation/actions";

import connect from "react-redux/es/connect/connect";
import classNames from "./operations.module.css";
import FormAddEndDate from "../../generic/FormAddEndDate/FormAddEndDate";
import FormNewOperation from "../../generic/FormNewOperation/FormNewOperation";
import {Collapse, Typography, Icon, Modal, Button} from "antd";
import dateFormat from "dateformat";
import {Link} from "react-router-dom";


const { Title } = Typography;
const { Panel } = Collapse;


class Operations extends React.PureComponent {

    state = {
        idProbleme: null,
        visible: false,
        itemSelected: null,
        dateSelected: null,
        pieceToAdd: null,
    }

    componentWillMount() {
        this.props.getPendingOperationInProbleme(this.props.idProbleme);
        this.props.getProblemeById(this.props.idProbleme)

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
      if(prevProps.idProbleme !== this.props.idProbleme){
        this.setState({
            idProbleme: this.props.idProbleme,
        }, () => {
          this.props.getPendingOperationInProbleme(this.state.idProbleme)
        })
      }
      if(prevProps.newOperation !== this.props.newOperation){
        this.setState({
          newOperation: this.props.newOperation,
        }, () => {
          if(this.props.newOperation[0]){
            if(this.props.newOperation[0].insertId) {
              //console.log("coucou", this.props.newOperation[0])
              if(this.state.pieceToAdd){
                this.props.postPieceToOperation(this.props.newOperation[0].insertId, this.state.pieceToAdd)
              }
            }
          }})
      }
    }

    addOperation = (idProbleme, values, piece) => {
        this.props.postOperationInProbleme(idProbleme, values)
        this.setState({
          pieceToAdd: piece,
        })
        this.props.getPendingOperationInProbleme(this.props.idProbleme)
    }
    hideModal = () => {
        this.setState({
            visible: false,
        })
    }
    showModal = () => {
        this.setState({
            visible: true,

        })
    }
    validateArchive = (idOperation, endDate) => {
        this.props.addEndDateOperation(idOperation, endDate.endDate)
        this.props.getPendingOperationInProbleme(this.props.idProbleme)
        this.hideModal()
    }


    render() {
        return (
          <div>
              <div>
                <Title className={classNames.title}> Opérations de maintenances </Title>
                <Title className={classNames.title} level={2}>Problème :  {this.props.probleme.map((item) => {return item.titre})}</Title>
              </div>
              <FormNewOperation problemeId={this.props.probleme.map((item) => {return item.id})} addOperation={this.addOperation}/>
              <div className={classNames.problemepart}>
                <Title className={classNames.title} level={2}>Liste des Opérations de maintenance en cours</Title>
                <div className={classNames.collapses}>
                  <Collapse>
                    {this.props.operations.map((item) =>{
                      return (
                        <Panel header={item.sujet} key={item.id} extra={this.genExtra(item)}>
                            <h5>Description de l'opération</h5>
                            <p>{item.description}</p>
                          <Link to={"operations/" +item.id}><Button><h4>Détails de l'opération</h4></Button></Link>
                        </Panel>
                      )
                    })}
                  </Collapse>
                    <Modal
                        title="Voulez-vous archiver cette opération ?"
                        visible={this.state.visible}
                        onCancel={this.hideModal}
                        footer={null}
                        cancelText="Cancel"
                    >
                        <h4>Sujet : {this.state.itemSelected ? this.state.itemSelected.sujet : "pas d'item sélectionné"}</h4>
                        <p>Description : {this.state.itemSelected ? this.state.itemSelected.description : "pas d'item sélectionné"}</p>
                        <p>Pour archiver cette opération complétez la date de fin</p>
                        <FormAddEndDate validateArchive={this.validateArchive} itemSelected={this.state.itemSelected}/>
                    </Modal>
                </div>
              </div>
          </div>
        )

    }

    genExtra = (item) => {
      return (
          <div className={classNames.extra}>
              <div className={classNames.datedebut}> démarré le {dateFormat(item.date_debut, "dd/mm/yyyy")}</div>
              <Icon
                  type="close"
                  onClick={(event) => {
                    event.stopPropagation();
                      this.setState({
                          itemSelected: item,
                      },() => {
                          this.showModal()
                      })
                  }}
                />

          </div>
      )
    }

}




const mapStateToProps = state  => ({
        probleme : state.probleme.map,
        operations: state.operations.map,
        pieces: state.pieces.map,
        newOperation: state.newOperation.map
    }
)

const mapDispatchToProps = {
    getProblemeById,
    getPendingOperationInProbleme,
    postOperationInProbleme,
    addEndDateOperation,
    postPieceToOperation,
}


export default connect(mapStateToProps, mapDispatchToProps)(Operations)
