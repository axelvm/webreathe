import React from "react";
import {getProblemes, postProbleme, deleteProbleme} from "../../../../redux/problemes/actions";
import {getPendingOperationInProbleme} from "../../../../redux/operations/actions";
import connect from "react-redux/es/connect/connect";
import classNames from "./probleme.module.css";
import {Collapse, Typography, Icon, Button} from "antd";
import FormNewProbleme from "../../generic/FormNewProbleme/FormNewProbleme";
import { Link } from "react-router-dom";


const { Title } = Typography;
const { Panel } = Collapse;


class Probleme extends React.PureComponent {

    state = {
        vehicule: null,
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
      if(prevProps.vehicule !== this.props.vehicule){
        this.setState({
          vehicule: this.props.vehicule,
        }, () => {
          this.props.getProblemes(this.state.vehicule.id)
        })
      }
    }



    render() {

        return (
          <div>
              <div>
                <Title className={classNames.title}> Bienvenue sur la fiche de détails du véhicule {this.props.vehicule.id} </Title>
                <Title className={classNames.title} level={2}>{this.props.vehicule.type} acheté le : {this.props.vehicule.date_achat}</Title>
              </div>
              <div className={classNames.problemepart}>
                <FormNewProbleme addProbleme={this.addProbleme}/>
                <Title className={classNames.title} level={2}>Liste des Problèmes de ce véhicule</Title>
                <div className={classNames.collapses}>
                  <Collapse onChange={this.getProblemDetails}>
                    {this.props.problemes.map((item) => {
                      return (
                        <Panel header={item.titre} key={item.id} extra={this.genExtra(item.id)}>
                            <p>{item.description}</p>
                            <div className={classNames.operationspart}>
                                <Link to={"/tableaudebord/vehicule/"+this.props.vehicule.id+"/problemes/"+ item.id +"/operations"}><Button><h4>Opérations de Maintenance en cours</h4></Button></Link>
                                {this.props.operations.map((item) => {
                                    return (
                                        <Button size="small" type="link">{item.sujet}</Button>
                                    )
                                })
                                }
                            </div>

                        </Panel>
                      )
                    })}
                  </Collapse>
                </div>
              </div>
          </div>
        )

    }
    getProblemDetails = (id) => {
        if(id.length){
            this.props.getPendingOperationInProbleme(id)
        }

    }
    genExtra = (id) => {
      return (
        <Icon
          type="close"
          onClick={(event) => {
            event.stopPropagation();
            this.props.deleteProbleme(id);
            this.props.getProblemes(this.state.vehicule.id)
          }}
        />
      )
    }
  addProbleme = (values) => {
    this.props.postProbleme(this.state.vehicule.id, values);
    this.props.getProblemes(this.state.vehicule.id);
  }
}




const mapStateToProps = state  => ({
        problemes : state.problemes.map,
        newProbleme: state.newProbleme.map,
        resultProbleme: state.resultProbleme.map,
        operations: state.operations.map
    }
)

const mapDispatchToProps = {
    getProblemes,
    postProbleme,
    deleteProbleme,
    getPendingOperationInProbleme,
}


export default connect(mapStateToProps, mapDispatchToProps)(Probleme)
