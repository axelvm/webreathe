import React from "react";

import { Redirect } from 'react-router-dom';
import EspaceAdmin from '../EspacesUtilisateur/EspaceAdmin/EspaceAdmin';
import EspaceGestionnaire from '../EspacesUtilisateur/EspaceGestionnaire/EspaceGestionnaire';
import EspaceTechnicien from '../EspacesUtilisateur/EspaceTechnicien/EspaceTechnicien';



export default class TableauDeBord extends React.PureComponent {


  state = {
    name: null,
    role: null,
    surname: null,
    isLogIn: false,
  };

  componentWillMount(){
    this.setState({
      name: this.props.name,
      surname: this.props.surname,
      role: this.props.role,
    })
  }
  checkConnection = () => {
    if(this.state.name && this.state.surname && this.state.role){
      return true;
    }else return false;
  }
  render() {

    if(!this.checkConnection()){
      return <Redirect to={'/'}/>;
    }else if (this.state.role === "admin"){
      return (
        <EspaceAdmin name={this.state.name} surname={this.state.surname}/>
      );
    }else if (this.state.role === "gestionnaire"){
      return (
        <EspaceGestionnaire name={this.state.name} surname={this.state.surname}/>
      );
    }else if (this.state.role === "technicien"){
      return (
        <EspaceTechnicien name={this.state.name} surname={this.state.surname}/>
      );
    }

  }
}