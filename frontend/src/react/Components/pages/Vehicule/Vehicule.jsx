import React from "react";
import {getVehiculeById} from "../../../../redux/vehicules/actions";
import connect from "react-redux/es/connect/connect";
import Probleme from "../Probleme/Probleme";



class Vehicule extends React.PureComponent {

    state = {
        vehicule: null,
    }
    componentWillMount() {
        this.props.getVehiculeById(this.props.id);
        this.setState({
            vehicule: this.props.vehicule
        })
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.vehicule !== this.props.vehicule){
            this.setState({
                vehicule: this.props.vehicule[0],
            })
        }
    }

    render() {

        return (
            <div>
                <Probleme vehicule={this.state.vehicule}/>
            </div>
        )

    }

}


const mapStateToProps = state  => ({
        vehicule : state.vehicule.map,
    }
)

const mapDispatchToProps = {
    getVehiculeById,
}


export default connect(mapStateToProps, mapDispatchToProps)(Vehicule)
