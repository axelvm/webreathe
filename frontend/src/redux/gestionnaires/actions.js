import { ACTIONS } from "./constants";
const request = require('superagent');


export const GET_GESTIONNAIRES = (listGestionnaires) => ({
  type: ACTIONS.GET_GESTIONNAIRES,
  listGestionnaires
})


export const getGestionnaires = () => async (dispatch) => {

    //TODO : brancher la connexion à l'API
  request
    .get('/users/gestionnaires')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_GESTIONNAIRES(res.body))
    });
}





