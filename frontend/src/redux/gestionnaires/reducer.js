import { ACTIONS } from "./constants";
import {combineReducers} from "redux";



export default combineReducers({
    map : (state= [], action) => {
        switch (action.type) {
            case ACTIONS.GET_GESTIONNAIRES :
                return action.listGestionnaires;
            default:
                return state
        }
    }
})


