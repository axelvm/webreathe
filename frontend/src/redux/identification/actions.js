import { ACTIONS } from "./constants";
const request = require('superagent');


export const CONNECT_USER = (isLogIn) => ({
  type: ACTIONS.CONNECT_USER,
  isLogIn
})


export const connectUser = (values) => async (dispatch) => {

    //TODO : brancher la connexion à l'API
  request
    .get('/user/'+values.name+'/'+values.surname)
    .set('Accept', 'application/json')
    .then(res => {
      if(res.text !== "[]"){
        dispatch(CONNECT_USER(res.body))
      }else{
        dispatch(CONNECT_USER(false))
      }

    });
}





