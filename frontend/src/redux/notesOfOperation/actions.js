import { ACTIONS } from "./constants";
const request = require('superagent');


export const GET_NOTES_OF_OPERATION = (notesOfOperation) => ({
  type: ACTIONS.GET_NOTES_OF_OPERATION,
  notesOfOperation
})
export const POST_NOTE_TO_OPERATION = (newNotesOfOperation) => ({
  type: ACTIONS.POST_NOTE_TO_OPERATION,
  newNotesOfOperation
})


export const postNoteToOperation = (id, note) => async (dispatch) => {

  request
    .post('/operations/'+id+'/notes')
    .send({
      "id_technicien": note.id_technicien,
      "note": note.note
    }).set('Accept', 'application/json')
    .then(res => {
      dispatch(POST_NOTE_TO_OPERATION([res.body]))
    });
}

export const getNotesOfOperation = (id) => async (dispatch) => {

  //TODO : brancher la connexion à l'API
  request
    .get('/operations/'+id+'/notes')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_NOTES_OF_OPERATION(res.body))
    });
}







