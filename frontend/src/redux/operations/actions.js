import { ACTIONS } from "./constants";
const request = require('superagent');

export const GET_PENDING_OPERATION_IN_PROBLEME = (operations) => ({
    type: ACTIONS.GET_PENDING_OPERATION_IN_PROBLEME,
    operations
});

export const POST_OPERATION_IN_PROBLEME = (newOperation) => ({
    type: ACTIONS.POST_OPERATION_IN_PROBLEME,
    newOperation
});

export const ADD_END_DATE_TO_OPERATION = (endedOperation) => ({
    type: ACTIONS.ADD_END_DATE_TO_OPERATION,
    endedOperation
})

export const GET_OPERATION_BY_ID = (operation) => ({
  type: ACTIONS.GET_OPERATION_BY_ID,
  operation
})

export const getOperationById = (id) => async (dispatch) => {

  //TODO : brancher la connexion à l'API
  request
    .get('/operations/'+id)
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_OPERATION_BY_ID(res.body))
    });
}



export const getPendingOperationInProbleme = (id) => async (dispatch) => {

  //TODO : brancher la connexion à l'API
  request
    .get('/problemes/'+id+'/operations')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_PENDING_OPERATION_IN_PROBLEME(res.body))
    });
}
export const postOperationInProbleme = (id, operation) => async (dispatch) => {

    request
        .post('/problemes/'+id+'/operations')
        .send({
            "sujet": operation.sujet,
            "description": operation.description,
            "date_debut": operation.date_debut
        }).set('Accept', 'application/json')
        .then(res => {

            dispatch(POST_OPERATION_IN_PROBLEME([res.body]))
        });
}


export const addEndDateOperation = (id, date_fin) => async (dispatch) => {

    request
        .put('/operations/' + id + '/datefin')
        .send({
            "date_fin": date_fin,
        }).set('Accept', 'application/json')
        .then(res => {
            dispatch(ADD_END_DATE_TO_OPERATION([res.body]))
        });
}

