import { ACTIONS } from "./constants";
import {combineReducers} from "redux";



export default combineReducers({
    map : (state= [], action) => {
        switch (action.type) {
          case ACTIONS.GET_PENDING_OPERATION_IN_PROBLEME :
            return action.operations;
          case ACTIONS.POST_OPERATION_IN_PROBLEME :
            return action.newOperation;
          case ACTIONS.ADD_END_DATE_TO_OPERATION :
            return state;
          case ACTIONS.GET_OPERATION_BY_ID :
            return action.operation
            default:
                return state
        }
    }
})


