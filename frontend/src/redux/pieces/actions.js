import { ACTIONS } from "./constants";
const request = require('superagent');
export const POST_PIECE = () => ({
  type: ACTIONS.POST_PIECE,

})
export const GET_PIECES = (pieces) => ({
  type: ACTIONS.GET_PIECES,
  pieces
})
export const GET_PIECE_BY_ID = (piece) => ({
    type: ACTIONS.GET_PIECE_BY_ID,
  piece
})



export const postPiece = (piece) => async (dispatch) => {
  request
    .post('/pieces')
    .send({
      "nom": piece,
    }).set('Accept', 'application/json')
    .then(res => {
      dispatch(POST_PIECE())
    });
}

export const getPieces = () => async (dispatch) => {

  //TODO : brancher la connexion à l'API
  request
    .get('/pieces')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_PIECES(res.body))
    });
}


export const getPieceById = (id) => async (dispatch) => {

    //TODO : brancher la connexion à l'API
    request
        .get('/pieces/'+id)
        .set('Accept', 'application/json')
        .then(res => {
            dispatch(GET_PIECE_BY_ID(res.body))
        });
}






