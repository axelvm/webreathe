import { ACTIONS } from "./constants";
import {combineReducers} from "redux";



export default combineReducers({
    map : (state= [], action) => {
        switch (action.type) {
          case ACTIONS.POST_PIECE :
            return state;
            case ACTIONS.GET_PIECES :
                return action.pieces;
          case ACTIONS.GET_PIECE_BY_ID :
            return action.piece;
            default:
                return state
        }
    }
})


