import { ACTIONS } from "./constants";
const request = require('superagent');


export const GET_PIECES_OF_OPERATION = (piecesOfOperation) => ({
  type: ACTIONS.GET_PIECES_OF_OPERATION,
  piecesOfOperation
})
export const ADD_PIECE_TO_OPERATION = (newPiecesOfOperation) => ({
  type: ACTIONS.ADD_PIECE_TO_OPERATION,
  newPiecesOfOperation
})


export const postPieceToOperation = (id, piece) => async (dispatch) => {

  request
    .post('/operations/'+id+'/pieces')
    .send({
      "id_piece": piece
    }).set('Accept', 'application/json')
    .then(res => {
      dispatch(ADD_PIECE_TO_OPERATION([res.body]))
    });
}

export const getPiecesOfOperation = (id) => async (dispatch) => {

  //TODO : brancher la connexion à l'API
  request
    .get('/operations/'+id+'/pieces')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_PIECES_OF_OPERATION(res.body))
    });
}







