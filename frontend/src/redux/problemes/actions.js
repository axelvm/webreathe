import { ACTIONS } from "./constants";
const request = require('superagent');


export const POST_PROBLEME = (newProbleme) => ({
  type: ACTIONS.POST_PROBLEME,
  newProbleme
})
export const GET_PROBLEMES = (problemes) => ({
  type: ACTIONS.GET_PROBLEMES,
  problemes
})
export const GET_PROBLEME_BY_ID = (probleme) => ({
    type: ACTIONS.GET_PROBLEME_BY_ID,
    probleme
})
export const DELETE_PROBLEME = (resultProbleme) => ({
  type: ACTIONS.DELETE_PROBLEME,
  resultProbleme
})



export const getProblemes = (id) => async (dispatch) => {

  //TODO : brancher la connexion à l'API
  request
    .get('/vehicules/'+id+'/problemes')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_PROBLEMES(res.body))
    });
}

export const getProblemeById = (id) => async (dispatch) => {

    //TODO : brancher la connexion à l'API
    request
        .get('/problemes/'+id)
        .set('Accept', 'application/json')
        .then(res => {
            dispatch(GET_PROBLEME_BY_ID(res.body))
        });
}

export const postProbleme = (id, probleme) => async (dispatch) => {

  request
    .post('/vehicules/'+id+'/problemes')
    .send({
      "titre": probleme.titre,
      "description": probleme.description,
    }).set('Accept', 'application/json')
    .then(res => {
      dispatch(POST_PROBLEME([res.body]))
    });
}

export const deleteProbleme = (id) => async (dispatch) => {

  request
    .delete('/problemes/'+id)
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(DELETE_PROBLEME([res.body]))
    });
}




