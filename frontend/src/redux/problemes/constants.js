export const ACTIONS = {
  POST_PROBLEME : 'POST_PROBLEME',
  DELETE_PROBLEME : 'DELETE_PROBLEME',
  GET_PROBLEMES : 'GET_PROBLEMES',
  GET_PROBLEME_BY_ID: 'GET_PROBLEME_BY_ID',
}
