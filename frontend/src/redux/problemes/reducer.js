import { ACTIONS } from "./constants";
import {combineReducers} from "redux";



export default combineReducers({
    map : (state= [], action) => {
        switch (action.type) {
          case ACTIONS.GET_PROBLEMES :
            return action.problemes;
            case ACTIONS.GET_PROBLEME_BY_ID :
                return action.probleme;
          case ACTIONS.POST_PROBLEME :
            return action.newProbleme;
          case ACTIONS.DELETE_PROBLEME :
            return action.resultProbleme;
            default:
                return state
        }
    }
})


