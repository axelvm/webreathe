import { combineReducers } from 'redux'
import identificationReducer from './identification/reducer'
import gestionnairesReducer from './gestionnaires/reducer'
import techniciensReducer from './techniciens/reducer'
import utilisateursReducer from './utilisateurs/reducer'
import vehiculesReducer from './vehicules/reducer'
import problemesReducer from './problemes/reducer'
import operationsReducer from './operations/reducer'
import piecesReducer from './pieces/reducer'
import piecesOfOperationReducer from './piecesOfOperation/reducer'
import notesOfOperationReducer from './notesOfOperation/reducer'

export default combineReducers({
  isLogIn : identificationReducer,
  listGestionnaires: gestionnairesReducer,
  listTechniciens: techniciensReducer,
  newUser: utilisateursReducer,
  result: utilisateursReducer,
  vehicules: vehiculesReducer,
  vehicule: vehiculesReducer,
  newVehicules: vehiculesReducer,
  resultVehicule: vehiculesReducer,
  problemes: problemesReducer,
  newProbleme: problemesReducer,
  resultProbleme: problemesReducer,
  operations: operationsReducer,
  newOperation: operationsReducer,
  operation: operationsReducer,
  probleme: problemesReducer,
  endedOperation : operationsReducer,
  newPiece: piecesReducer,
  pieces: piecesReducer,
  piece: piecesReducer,
  piecesOfOperation: piecesOfOperationReducer,
  newPiecesOfOperation: piecesOfOperationReducer,
  notesOfOperation: notesOfOperationReducer,
})

