import { ACTIONS } from "./constants";
const request = require('superagent');


export const GET_TECHNICIENS = (listTechniciens) => ({
  type: ACTIONS.GET_TECHNICIENS,
  listTechniciens,
})


export const getTechniciens = () => async (dispatch) => {

  request
    .get('/users/techniciens')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_TECHNICIENS(res.body))
    });
}





