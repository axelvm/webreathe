import { ACTIONS } from "./constants";
import {combineReducers} from "redux";



export default combineReducers({
    map : (state= [], action) => {
        switch (action.type) {
            case ACTIONS.GET_TECHNICIENS :
                return action.listTechniciens;
            default:
                return state
        }
    }
})


