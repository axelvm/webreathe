import { ACTIONS } from "./constants";
const request = require('superagent');


export const POST_USER = (newUser) => ({
  type: ACTIONS.POST_USER,
  newUser
})
export const DELETE_USER = (result) => ({
  type: ACTIONS.DELETE_USER,
  result
})



export const deleteUser = (id) => async (dispatch) => {

  request
    .delete('/user/'+id)
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(DELETE_USER(res.body))
    });
}


export const postUser = (user) => async (dispatch) => {

  request
    .post('/users')
    .send(
      {
        "name": user.name,
        "surname": user.surname,
        "role": user.role
      }
    )
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(POST_USER(res.body))
    });
}






