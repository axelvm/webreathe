import { ACTIONS } from "./constants";
import {combineReducers} from "redux";



export default combineReducers({
    map : (state= [], action) => {
        switch (action.type) {
            case ACTIONS.POST_USER :
            return action.newUser;
          case ACTIONS.DELETE_USER :
            return action.result;

            default:
                return state
        }
    }
})


