import { ACTIONS } from "./constants";
const request = require('superagent');


export const POST_VEHICULES = (newVehicules) => ({
  type: ACTIONS.POST_VEHICULES,
  newVehicules
})
export const GET_VEHICULES = (vehicules) => ({
  type: ACTIONS.GET_VEHICULES,
  vehicules
})
export const GET_VEHICULE_BY_ID = (vehicule) => ({
    type: ACTIONS.GET_VEHICULE_BY_ID,
    vehicule
})
export const DELETE_VEHICULE = (resultVehicule) => ({
  type: ACTIONS.DELETE_VEHICULE,
  resultVehicule
})



export const getVehicules = () => async (dispatch) => {

  //TODO : brancher la connexion à l'API
  request.get('/vehicules')
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(GET_VEHICULES(res.body))
    });
}

export const getVehiculeById = (id) => async  (dispatch) => {
    request
        .get('/vehicule/'+id)
        .set('Accept', 'application/json')
        .then(res => {
            dispatch(GET_VEHICULE_BY_ID(res.body))
        });
}
export const deleteVehicule = (id) => async (dispatch) => {

  request
    .delete('/vehicule/'+id)
    .set('Accept', 'application/json')
    .then(res => {
      dispatch(DELETE_VEHICULE([res.body]))
    });
}


export const postVehicules = (vehicule) => async (dispatch) => {

  request
    .post('/vehicules')
    .send({
        "type": vehicule.type,
        "date_achat": vehicule.date_achat,
      }).set('Accept', 'application/json')
    .then(res => {
      dispatch(POST_VEHICULES([res.body]))
    });
}






