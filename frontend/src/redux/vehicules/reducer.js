import { ACTIONS } from "./constants";
import {combineReducers} from "redux";



export default combineReducers({
    map : (state= [], action) => {
        switch (action.type) {
            case ACTIONS.POST_VEHICULES :
            return action.newVehicules;
          case ACTIONS.DELETE_VEHICULE :
            return action.resultVehicule;
          case ACTIONS.GET_VEHICULES :
            return action.vehicules;
          case ACTIONS.GET_VEHICULE_BY_ID :
            return action.vehicule;
            default:
                return state
        }
    }
})


